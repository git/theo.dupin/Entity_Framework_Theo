﻿using Model;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;

namespace Web_Api
{
    public class LargeImageDTO
    {
        public int Id { get; set; }
        public string Base64 { get; set; }

        public LargeImage toModel()
        {
            return new LargeImage(Base64);
        }
    }
}
