using EntityFrameworkLib;
using Microsoft.AspNetCore.Routing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using StubLib;
using System.Diagnostics;
using static StubLib.StubData;

namespace UnitTestsSQLiteInMemory
{
    public class ChampionsDbTests
    {

        StubData data = new StubData();

        [Fact]
        public void Add_Champions_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolContext>()
                .UseInMemoryDatabase(databaseName: "TestChampions_database")
                .Options;

            using(var context = new LolContext(options)) 
            {
                ChampionEntity akali = new ChampionEntity { Name = "Akali", Bio = "Bio Akali", Icon = "Icon Akali", Class = ChampionClass.Assassin };
                ChampionEntity alistar = new ChampionEntity { Name = "Alistar", Bio = "Bio Alistar", Icon = "Icon Alistar", Class = ChampionClass.Support };
                ChampionEntity bard = new ChampionEntity { Name = "Bard", Bio = "Bio Bard", Icon = "Icon Bard", Class = ChampionClass.Support };

                context.Champions.Add(akali);
                context.Champions.Add(alistar);
                context.Champions.Add(bard);
                context.SaveChanges();
            }

            using(var context = new LolContext(options))
            {
                Assert.Equal(3, context.Champions.Count());
                Assert.Equal("Akali", context.Champions.First().Name);
            }
        }

        [Fact]
        public void Modify_Champions_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolContext(options))
            {
                //context.Database.OpenConnection();
                context.Database.EnsureCreated();

                var championToUpdate = context.Champions.SingleOrDefault(c => c.Name == "Akali");

                if (championToUpdate != null)
                {
                    var newChampion = new ChampionEntity
                    {
                        Name = "akali",
                        Bio = championToUpdate.Bio,
                        Icon = championToUpdate.Icon,
                        Class = championToUpdate.Class
                    };

                    string nameToFind = "i";
                    Assert.Equal(1, context.Champions.Where(n => n.Name.ToLower().Contains(nameToFind)).Count());
                    nameToFind = "aka";
                    Assert.Equal(1, context.Champions.Where(n => n.Name.ToLower().Contains(nameToFind)).Count());

                    context.Champions.Remove(championToUpdate);

                    context.Champions.Add(newChampion);

                    context.SaveChanges();
                }
            }
        }

        [Fact]
        public void Delete_Champions_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolContext(options))
            {
                //context.Database.OpenConnection();
                context.Database.EnsureCreated();

                ChampionEntity veigar = new ChampionEntity { Name = "Veigar", Bio = "Bio Veigar", Icon = "Icon Veigar", Class = ChampionClass.Fighter };
                ChampionEntity yone = new ChampionEntity { Name = "Yone", Bio = "Bio Yone", Icon = "Icon Yone", Class = ChampionClass.Assassin };
                ChampionEntity yasuo = new ChampionEntity { Name = "Yasuo", Bio = "Bio Yasuo", Icon = "icon Yasuo", Class = ChampionClass.Assassin };

                context.Champions.Add(veigar);
                context.Champions.Add(yone);
                context.Champions.Add(yasuo);

                context.SaveChanges();

                context.Champions.Remove(veigar);
                context.Champions.Remove(yone);
                context.Champions.Remove(yasuo);

                context.SaveChanges();
            }
        }

        [Fact]
        public async Task TestAddAndGetChampion()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<LolContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new LolContext(options))
                {
                    context.Database.EnsureCreated();
                }
                var manager = new ChampionsManager(data);

                var champion = new Champion("TestChampion", ChampionClass.Mage);
                var addedChampion = await manager.AddItem(champion);

                Assert.Equal(champion, addedChampion);

                var nbChampions = await manager.GetNbItems();
                Assert.Equal(7, nbChampions);

                var champions = await manager.GetItems(0, nbChampions);
                Assert.Equal(champion, champions.Last());
            }
            finally
            {
                connection.Close();
            }
        }

        [Fact]
        public async Task TestGetChampionsByClass()
        {
            var manager = new ChampionsManager(data);

            var nbAssassins = await manager.GetNbItemsByClass(ChampionClass.Assassin);
            Assert.Equal(1, nbAssassins);

            var assassins = await manager.GetItemsByClass(ChampionClass.Assassin, 0, nbAssassins, null);
            Assert.Equal("Akali", assassins.First().Name);

            var nbMages = await manager.GetNbItemsByClass(ChampionClass.Mage);
            Assert.Equal(1, nbMages);

            var mages = await manager.GetItemsByClass(ChampionClass.Mage, 0, nbMages, "Name", descending: true);
            Assert.Equal("Ahri", mages.First().Name);
        }

        [Fact]
        public async Task TestAddAndGetSkinsByName()
        {
            var manager = new SkinsManager(data);

            var champion = new Champion("Jax", ChampionClass.Fighter);
            var skin = new Skin("Jaximus", champion);
            var addSkin = await manager.AddItem(skin);
            Assert.Equal(skin, addSkin);

            var nbSkins = await manager.GetNbItemsByName("Jaximus");
            Assert.Equal(1, nbSkins);
        }

        [Fact]
        public async Task TestDeleteAndGetSkins()
        {
            var manager = new SkinsManager(data);

            var champion = new Champion("Jax", ChampionClass.Fighter);
            var skin = new Skin("Jaximus", champion);
            var addSkin = await manager.AddItem(skin);
            Assert.Equal(skin, addSkin);
            var isDelete = await manager.DeleteItem(skin);
            Assert.True(isDelete);
        }
    }
}