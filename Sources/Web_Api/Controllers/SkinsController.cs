﻿using Microsoft.AspNetCore.Mvc;
using StubLib;
using Web_Api.Mapper;
using static StubLib.StubData;

namespace Web_Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SkinsController : Controller
    {
        private SkinsManager SkinsManager { get; set; } = new StubData.SkinsManager(new StubData());

        private readonly ILogger<SkinsController> _logger;

        public SkinsController(ILogger<SkinsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("GET")]
        public async Task<IActionResult> Get()
        {
            var list = await SkinsManager.GetItems(0, await SkinsManager.GetNbItems());
            return Ok(list.Select(skin => skin?.toDTO()));
        }

        [HttpGet("GETBYNAME")]
        public async Task<IActionResult> GetByName(string name)
        {
            var skinsSelected = await SkinsManager.GetItemsByName(name, 0, await SkinsManager.GetNbItemsByName(name), null);
            if (skinsSelected.Count() == 0)
            {
                Console.WriteLine("Aucun skin ne correspond au nom { " + name + " } !");
                throw new Exception("Aucun skin ne correspond au nom { " + name + " } !");
            }
            Console.WriteLine("Le skin { " + name + " } existe");
            return Ok(skinsSelected.Select(skins => skins?.toDTO()));
        }

        [HttpPost("ADD")]
        public async Task<IActionResult> AddSkin(SkinDTO skin)
        {
            var newSkin = skin.toModel();
            await SkinsManager.AddItem(newSkin);
            if(skin.Description == "string")
            {
                skin.Description = "Aucune bio";
            }
            Console.WriteLine("Le skin { " + skin.Name + " } avec pour description { " + skin.Description + " } a bien été ajouté");
            return Ok(newSkin);
        }

        [HttpPut("UPDATE")]
        public async Task<IActionResult> UpdateChampion(string name, SkinDTO skin)
        {
            var skinSelected = await SkinsManager.GetItemsByName(name, 0, await SkinsManager.GetNbItemsByName(name), null);
            var existingSkin = skinSelected.FirstOrDefault();
            if(existingSkin == null)
            {
                return NotFound("Le skin { " + name + " } n'existe pas !");
            }

            var updatedSkin = skin.toModel();
            await SkinsManager.UpdateItem(existingSkin, updatedSkin);
            if(skin.Description == "string")
            {
                skin.Description = "Aucune bio";
            }
            return Ok("Le skin { " + name + " } a été modifié en " + " { " + updatedSkin.Name + " } avec pour description { " + updatedSkin.Description + " }<");
        }

        [HttpDelete("DELETE")]
        public async Task<IActionResult> DeleteChampion(string name)
        {
            var skinSelected = await SkinsManager.GetItemsByName(name, 0, await SkinsManager.GetNbItemsByName(name), null);
            if (!await SkinsManager.DeleteItem(skinSelected.FirstOrDefault()))
            {
                return NotFound("skin { " + name + " } non trouvé !");
            }
            Console.WriteLine("skin { " + name + " } supprimé");
            return Ok();
        }
    }
}
