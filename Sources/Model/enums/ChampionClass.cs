﻿using System;
using System.Text.Json.Serialization;

namespace Model
{
	public enum ChampionClass
	{
		Unknown,
		Assassin,
		Fighter,
		Mage,
		Marksman,
		Support,
		Tank,
	}
}

