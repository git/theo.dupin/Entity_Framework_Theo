﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkLib.Mappers
{
    public static class SkillChanger
    {
        public static SkillEntity toEntity(this Skill skill, ChampionEntity championEntity, LolContext context)
        {
            var skillEntity = context.Skills.Find(skill.Name);
            if (skillEntity == null)
            {
                return new()
                {
                    Name = skill.Name,
                    Description = skill.Description,
                    Type = skill.Type,
                    Champions = new List<ChampionEntity>() { championEntity }
                };
            }
            skillEntity!.Champions?.Add(championEntity);
            return skillEntity;
        }

        public static Skill toModel(this SkillEntity skillEntity)
         => new(skillEntity.Name, skillEntity.Type, skillEntity.Description);
    }
}
