﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ConsoleDbSQLite.Migrations
{
    /// <inheritdoc />
    public partial class AllMigrations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RunesPage",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RunesPage", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 500, nullable: false),
                    Type = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "t_LargeImage",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Base64 = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_LargeImage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Champions",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    Bio = table.Column<string>(type: "TEXT", maxLength: 500, nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: false),
                    Class = table.Column<int>(type: "INTEGER", nullable: false),
                    ImageId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Champions", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Champions_t_LargeImage_ImageId",
                        column: x => x.ImageId,
                        principalTable: "t_LargeImage",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Runes",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 500, nullable: false),
                    RuneFamily = table.Column<int>(type: "INTEGER", nullable: false),
                    ImageId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Runes", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Runes_t_LargeImage_ImageId",
                        column: x => x.ImageId,
                        principalTable: "t_LargeImage",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ChampionEntityRunePageEntity",
                columns: table => new
                {
                    ChampionsName = table.Column<string>(type: "TEXT", nullable: false),
                    RunePagesName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChampionEntityRunePageEntity", x => new { x.ChampionsName, x.RunePagesName });
                    table.ForeignKey(
                        name: "FK_ChampionEntityRunePageEntity_Champions_ChampionsName",
                        column: x => x.ChampionsName,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChampionEntityRunePageEntity_RunesPage_RunePagesName",
                        column: x => x.RunePagesName,
                        principalTable: "RunesPage",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChampionEntitySkillEntity",
                columns: table => new
                {
                    ChampionsName = table.Column<string>(type: "TEXT", nullable: false),
                    SkillsName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChampionEntitySkillEntity", x => new { x.ChampionsName, x.SkillsName });
                    table.ForeignKey(
                        name: "FK_ChampionEntitySkillEntity_Champions_ChampionsName",
                        column: x => x.ChampionsName,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChampionEntitySkillEntity_Skills_SkillsName",
                        column: x => x.SkillsName,
                        principalTable: "Skills",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Characteristics",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    ChampionForeignKey = table.Column<string>(type: "TEXT", nullable: false),
                    Value = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characteristics", x => new { x.Name, x.ChampionForeignKey });
                    table.ForeignKey(
                        name: "FK_Characteristics_Champions_ChampionForeignKey",
                        column: x => x.ChampionForeignKey,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Skins",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 500, nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: false),
                    Price = table.Column<float>(type: "REAL", nullable: false),
                    ChampionForeignKey = table.Column<string>(type: "TEXT", nullable: false),
                    ImageId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skins", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Skins_Champions_ChampionForeignKey",
                        column: x => x.ChampionForeignKey,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Skins_t_LargeImage_ImageId",
                        column: x => x.ImageId,
                        principalTable: "t_LargeImage",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "RunePageRunes",
                columns: table => new
                {
                    RuneName = table.Column<string>(type: "TEXT", nullable: false),
                    RunePageName = table.Column<string>(type: "TEXT", nullable: false),
                    Category = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RunePageRunes", x => new { x.RunePageName, x.RuneName });
                    table.ForeignKey(
                        name: "FK_RunePageRunes_RunesPage_RunePageName",
                        column: x => x.RunePageName,
                        principalTable: "RunesPage",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RunePageRunes_Runes_RuneName",
                        column: x => x.RuneName,
                        principalTable: "Runes",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChampionEntityRunePageEntity_RunePagesName",
                table: "ChampionEntityRunePageEntity",
                column: "RunePagesName");

            migrationBuilder.CreateIndex(
                name: "IX_ChampionEntitySkillEntity_SkillsName",
                table: "ChampionEntitySkillEntity",
                column: "SkillsName");

            migrationBuilder.CreateIndex(
                name: "IX_Champions_ImageId",
                table: "Champions",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Characteristics_ChampionForeignKey",
                table: "Characteristics",
                column: "ChampionForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_RunePageRunes_RuneName",
                table: "RunePageRunes",
                column: "RuneName");

            migrationBuilder.CreateIndex(
                name: "IX_Runes_ImageId",
                table: "Runes",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Skins_ChampionForeignKey",
                table: "Skins",
                column: "ChampionForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Skins_ImageId",
                table: "Skins",
                column: "ImageId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChampionEntityRunePageEntity");

            migrationBuilder.DropTable(
                name: "ChampionEntitySkillEntity");

            migrationBuilder.DropTable(
                name: "Characteristics");

            migrationBuilder.DropTable(
                name: "RunePageRunes");

            migrationBuilder.DropTable(
                name: "Skins");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "RunesPage");

            migrationBuilder.DropTable(
                name: "Runes");

            migrationBuilder.DropTable(
                name: "Champions");

            migrationBuilder.DropTable(
                name: "t_LargeImage");
        }
    }
}
