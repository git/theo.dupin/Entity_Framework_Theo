﻿using Model;
using Web_Api.Controllers;

namespace Web_Api.Mapper
{
    public static class ChampionMapper
    {
        public static ChampionDTO toDTO(this Champion champion)
        {
            return new ChampionDTO()
            {
                Name = champion.Name,
                Bio = champion.Bio
            };
        }
    }
}
