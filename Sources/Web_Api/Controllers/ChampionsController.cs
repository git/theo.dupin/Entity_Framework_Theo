﻿using EntityFrameworkLib;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Model;
using StubLib;
using Web_Api.Mapper;
using static StubLib.StubData;

namespace Web_Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChampionsController : ControllerBase
    {

        private ChampionsManager ChampionsManager { get; set; } = new StubData.ChampionsManager(new StubData());

        private readonly LolContext _context;

        public ChampionsController(LolContext context)
        {
            _context = context;
        }

        //private StubData.ChampionsManager ChampionsManager { get; set; } = new StubData.ChampionsManager(new StubData());

        private readonly ILogger<ChampionsController> _logger;

        public ChampionsController(ILogger<ChampionsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("GET")]
        public async Task<IActionResult> Get()
        {
            var list = await ChampionsManager.GetItems(0, await ChampionsManager.GetNbItems());
            return Ok(list.Select(rune => rune?.toDTO()));
            //Faire en sorte d'avoir un ChampionDTO et pas ChampionENtity
        }

        [HttpGet("GETBYNAME")]
        public async Task<IActionResult> GetByName(string name)
        {
            var championSelected = await ChampionsManager.GetItemsByName(name, 0, await ChampionsManager.GetNbItemsByName(name), null);
            if (championSelected.Count() == 0)
            {
                return NotFound("Aucun champion ne correspond au nom { " + name + " } !");
            }
            Console.WriteLine("Le champion { " + name + " } existe");
            return Ok(championSelected.Select(champion => champion?.toDTO()));
        }

        [HttpPost("ADD")]
        public async Task<IActionResult> AddChampion([FromBody] ChampionDTO champion)
        {
            var newChampion = champion.toModel();
            await ChampionsManager.AddItem(newChampion);
            if(champion.Bio == "string")
            {
                champion.Bio = "Aucune bio";
            }
            return Ok("Le champion { " + champion.Name + " } avec pour bio { " + champion.Bio + " } a bien été ajouté");
        }

        [HttpPut("UPDATE")]
        public async Task<IActionResult> UpdateChampion(string name, [FromBody] ChampionDTO champion)
        {
            var championSelected = await ChampionsManager.GetItemsByName(name, 0, await ChampionsManager.GetNbItemsByName(name), null);
            var existingChampion = championSelected.FirstOrDefault();
            if (existingChampion == null)
            {
                return NotFound("Le champion { " + name + " } n'existe pas !");
            }
            var updatedChampion = champion.toModel();
            await ChampionsManager.UpdateItem(existingChampion, updatedChampion);
            if(updatedChampion.Bio == "string")
            {
                updatedChampion.Bio = "Aucune bio";
            }
            return Ok("Le champion { " + name + " } a été modifié en { " + updatedChampion.Name + " } avec pour bio { " + updatedChampion.Bio + " }");
        }

        [HttpDelete("DELETE")]
        public async Task<IActionResult> DeleteChampion(string name)
        {
            var championSelected = await ChampionsManager.GetItemsByName(name, 0, await ChampionsManager.GetNbItemsByName(name), null);
            if(!await ChampionsManager.DeleteItem(championSelected.FirstOrDefault()))
            {
                return NotFound("champion { " + name + " } non trouvé !");
            }
            return Ok("champion { " + name + " } supprimé");
        }
    }
}
