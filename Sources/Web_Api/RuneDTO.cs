﻿using Model;

namespace Web_Api
{
    public class RuneDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public LargeImage Image { get; set; }

        public Rune toModel()
        {
            return new Rune(Name, Description, Image);
        }
    }
}
