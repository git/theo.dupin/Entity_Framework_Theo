﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkLib.Mappers
{
    public static class SkinChanger
    {
        public static Skin toModel(this SkinEntity skinEntity)
        {
            return new Skin(skinEntity.Name, skinEntity.Description, skinEntity.Icon, skinEntity.Price  /*championEntity.Image, championEntity.Class*/);
        }

        public static SkinEntity toEntity(this Skin skin)
        {
            return new SkinEntity
            {
                Name = skin.Name,
                Description = skin.Description,
                Icon = skin.Icon,
                Price = skin.Price
                //Image = champion.Image,
                //Class = champion.Class,
            };
        }
    }
}
