﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StubLib;
using Web_Api.Mapper;
using static StubLib.StubData;

namespace Web_Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RunesController : Controller
    {
        private RunesManager RunesManager { get; set; } = new StubData.RunesManager(new StubData());
        
        private readonly ILogger<RunesController> _logger;

        public RunesController(ILogger<RunesController> logger)
        {
            _logger = logger;
        }

        [HttpGet("GET")]
        public async Task<IActionResult> Get()
        {
            var list = await RunesManager.GetItems(0, await RunesManager.GetNbItems());
            return Ok(list.Select(rune => rune?.toDTO()));
        }

        [HttpGet("GETBYNAME")]
        public async Task<IActionResult> GetByName(string name)
        {
            var runesSelected = await RunesManager.GetItemsByName(name, 0, await RunesManager.GetNbItemsByName(name), null);
            if (runesSelected.Count() == 0)
            {
                return NotFound("Aucune rune ne correspond au nom { " + name + " } !");
            }
            Console.WriteLine("La rune { " + name + " } existe");
            return Ok(runesSelected.Select(runes => runes?.toDTO()));
        }

        [HttpPost("ADD")]
        public async Task<IActionResult> AddRune(RuneDTO rune)
        {
            var newRune = rune.toModel();
            await RunesManager.AddItem(newRune);
            if(rune.Description == "string")
            {
                rune.Description = "Aucune bio";
            }
            Console.WriteLine("La rune { " + rune.Name + " } avec pour descrption { " + rune.Description + " } a bien été ajoutée");
            return Ok(newRune);
        }

        [HttpPut("UPDATE")]
        public async Task<IActionResult> UpdateRune(string name, RuneDTO rune)
        {
            var runeSelected = await RunesManager.GetItemsByName(name, 0, await RunesManager.GetNbItemsByName(name), null);
            var existingRune = runeSelected.FirstOrDefault();
            if (existingRune == null)
            {
                return NotFound("La rune { " + name + " } n'existe pas !");
            }
            var updatedRune = rune.toModel();
            await RunesManager.UpdateItem(existingRune, updatedRune);
            if (rune.Description == "string")
            {
                rune.Description = "Aucune bio";
            }
            return Ok("La rune { " + name + " } a été modifiée en { " + updatedRune.Name + " } avec pour description { " + rune.Description + " }");
        }

        [HttpDelete("DELETE")]
        public async Task<IActionResult> DeleteRune(string name)
        {
            var runeSelected = await RunesManager.GetItemsByName(name, 0, await RunesManager.GetNbItemsByName(name), null);
            if(!await RunesManager.DeleteItem(runeSelected.FirstOrDefault()))
            {
                return NotFound("rune { " + name + " } non trouvée !");
            }
            return Ok("rune { " + name + " } supprimée");
        }
    }
}
