﻿using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Model;

namespace EntityFrameworkLib
{
    public class LolContext : DbContext
    {
        public DbSet<ChampionEntity> Champions { get; set; }
        public DbSet<CharacteristicEntity> Characteristics { get; set; }
        public DbSet<SkinEntity> Skins { get; set; }
        public DbSet<SkillEntity> Skills { get; set; }
        public DbSet<LargeImageEntity> Images { get; set; }
        public DbSet<RuneEntity> Runes { get; set; }
        public DbSet<RunePageEntity> RunesPage { get; set; }
        public LolContext() { }
        public LolContext(DbContextOptions<LolContext> options)
            : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                base.OnConfiguring(options.UseSqlite($"DataSource=projet.Champions.db"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<LargeImageEntity>().Property(l => l.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<CharacteristicEntity>().HasKey(c => new { c.Name, c.ChampionForeignKey });

            //Relation one-to-one entre un champion et une largeImage : un champion n'a qu'une seule largeImage et une largeImage ne peut être attribuée qu'à un seul champion
            modelBuilder.Entity<ChampionEntity>()
                .HasOne(c => c.Image)
                .WithOne(i => i.Champion)
                .HasForeignKey<ChampionEntity>(c => c.ImageId);

            //Relation one-to-one : un skin n'a qu'une image et chaque image n'appartient qu'à un seul skin
            modelBuilder.Entity<SkinEntity>()
                .HasOne(s => s.Image)
                .WithOne(i => i.Skin)
                .HasForeignKey<SkinEntity>(s => s.ImageId);

            //Relation one-to-many : un champion a plusieurs skins mais chaque skin ne peut appartenir qu'à un seul champion
            modelBuilder.Entity<ChampionEntity>()
                .HasMany(c => c.Skins)
                .WithOne(s => s.Champion)
                .HasForeignKey(s => s.ChampionForeignKey);


            //Relation many-to-many entre les champions et pages de runes : Un Champion peut avoir plusieurs pages de runes et plusieurs pages de runes peuvent être définies sur un champion
            /*modelBuilder.Entity<ChampionEntity>()
                .HasMany(c => c.RunePages)
                .WithMany(r => r.Champions)
                .UsingEntity(x => x.ToTable("ChampionRunePages"));*/
        }
    }
}
