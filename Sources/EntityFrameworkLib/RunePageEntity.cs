﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.RunePage;

namespace EntityFrameworkLib
{
    public class RunePageEntity
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }
        /*public RuneEntity? this[Category category]
        {
            get { return Runes.ContainsKey(category) ? Runes[category] : null; }
            set { Runes[category] = value; }
        }*/
        //public Dictionary<Category, RuneEntity>? Runes { get; set; } 
        public ICollection<ChampionEntity>? Champions { get; set; }
    }
}
