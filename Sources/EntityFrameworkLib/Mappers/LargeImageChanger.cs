﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkLib.Mappers
{
    public static class LargeImageChanger
    {
        public static int Id { get; private set; }

        public static LargeImage toModel(this LargeImageEntity imageEntity)
        {
            return new LargeImage(imageEntity.Base64) { Id = imageEntity.Id };
        }

        public static LargeImageEntity toEntity(this LargeImage image)
        {
            return new LargeImageEntity
            {
                Id = image.Id,
                Base64 = image.Base64
            };
        }
    }
}
