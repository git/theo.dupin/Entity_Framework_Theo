﻿using Model;
using System.Runtime.CompilerServices;

namespace Web_Api.Mapper
{
    public static class SkinMapper
    {
        public static SkinDTO toDTO(this Skin skin)
        {
            return new SkinDTO()
            {
                Name = skin.Name,
                Description = skin.Description,
                Price = skin.Price
            };
        }
    }
}
