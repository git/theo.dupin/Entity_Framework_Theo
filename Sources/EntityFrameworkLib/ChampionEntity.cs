﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
using Model;

namespace EntityFrameworkLib
{
    public class ChampionEntity
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Bio { get; set; }
        public string Icon { get; set; }
        [Required]
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ChampionClass Class { get; set; }
        public ICollection<SkillEntity>? Skills { get; set; }
        public ICollection<SkinEntity>? Skins { get; set; }
        public ICollection<CharacteristicEntity>? Characteristics { get; set; }
        public ICollection<RunePageEntity>? RunePages { get; set; }

        public int? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public LargeImageEntity? Image { get; set; }
    }
}
