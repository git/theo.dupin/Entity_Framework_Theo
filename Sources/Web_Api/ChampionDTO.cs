﻿using Model;

namespace Web_Api
{
    public class ChampionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Icon { get; set; }
        public LargeImageDTO Image { get; set; }
        public ChampionClass Class { get; set; }


        public Champion toModel()
        {
            return new Champion(Name, Bio);
        }
    }
}
