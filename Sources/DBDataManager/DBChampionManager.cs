﻿using EntityFrameworkLib;
using EntityFrameworkLib.Mappers;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDataManager
{
    public class DBChampionManager : IChampionsManager
    {
        private readonly LolContext context;

        public DBChampionManager()
        {
        }

        public DBChampionManager(LolContext context)
        {
            this.context = context;
        }

        async Task<Champion?> IGenericDataManager<Champion?>.AddItem(Champion? item)
        {
            if(item == null)
            {
                return null;
            }
            var additem = await context.AddAsync<Champion?>(item);
            await context.SaveChangesAsync();
            return additem.Entity;
        }

        async Task<bool> IGenericDataManager<Champion?>.DeleteItem(Champion? item)
        {
            if(item == null)
            {
                return false;
            }
            context.Remove<Champion>(item);
            await context.SaveChangesAsync();
            return true;
        }

        async Task<IEnumerable<Champion?>> IGenericDataManager<Champion?>.GetItems(int index, int count, string? orderingPropertyName, bool descending)
        {
            return (IEnumerable<Champion?>)context.Champions.GetItemsWithFilterAndOrdering(c => true, index, count, orderingPropertyName, descending);
        }

        private Func<Champion, string, bool> filterByCharacteristic = (champ, charName) => champ.Characteristics.Keys.Any(k => k.Contains(charName, StringComparison.InvariantCultureIgnoreCase));

        Task<IEnumerable<Champion?>> IChampionsManager.GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName, bool descending)
            => context.Champions.toPocos().GetItemsWithFilterAndOrdering(
               champions => filterByCharacteristic(champions, charName),
               index, count, orderingPropertyName, descending);

        private Func<Champion, ChampionClass, bool> filterByClass = (champ, championClass) => champ.Class == championClass;

        Task<IEnumerable<Champion?>> IChampionsManager.GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName, bool descending)
            => context.Champions.toPocos().GetItemsWithFilterAndOrdering(
                champions => filterByClass(champions, championClass),
                index, count, orderingPropertyName, descending);

        private Func<Champion, string, bool> filterByName = (champ, substring) => champ.Name.Contains(substring, StringComparison.InvariantCultureIgnoreCase);

        Task<IEnumerable<Champion?>> IGenericDataManager<Champion?>.GetItemsByName(string substring, int index, int count, string? orderingPropertyName, bool descending)
            => context.Champions.toPocos().GetItemsWithFilterAndOrdering(
                champions => filterByName(champions, substring),
                index, count, orderingPropertyName, descending);

        Task<IEnumerable<Champion?>> IChampionsManager.GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName, bool descending)
        {
            throw new NotImplementedException();
        }


        private Func<Champion, Skill?, bool> filterBySkill = (champ, skill) => skill != null && champ.Skills.Contains(skill!);

        Task<IEnumerable<Champion?>> IChampionsManager.GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName, bool descending)
            => context.Champions.toPocos().GetItemsWithFilterAndOrdering(
                champions => filterBySkill(champions, skill),
                index, count, orderingPropertyName, descending);


        private static Func<Champion, string, bool> filterBySkillSubstring = (champ, skill) => champ.Skills.Any(s => s.Name.Contains(skill, StringComparison.InvariantCultureIgnoreCase));

        Task<IEnumerable<Champion?>> IChampionsManager.GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName, bool descending)
            => context.Champions.toPocos().GetItemsWithFilterAndOrdering(
                champions => filterBySkillSubstring(champions, skill),
                index, count, orderingPropertyName, descending);

        Task<int> IGenericDataManager<Champion?>.GetNbItems()
            => Task.FromResult(context.Champions.Count());

        Task<int> IChampionsManager.GetNbItemsByCharacteristic(string charName)
            => context.Champions.toPocos().GetNbItemsWithFilter(
                champions => filterByCharacteristic(champions, charName));

        Task<int> IChampionsManager.GetNbItemsByClass(ChampionClass championClass)
            => context.Champions.toPocos().GetNbItemsWithFilter(
                champions => filterByClass(champions, championClass));

        Task<int> IGenericDataManager<Champion?>.GetNbItemsByName(string substring)
            => context.Champions.toPocos().GetNbItemsWithFilter(
                champions => filterByName(champions, substring));

        Task<int> IChampionsManager.GetNbItemsByRunePage(RunePage? runePage)
        {
            throw new NotImplementedException();
        }

        Task<int> IChampionsManager.GetNbItemsBySkill(Skill? skill)
            => context.Champions.toPocos().GetNbItemsWithFilter(
                champions => filterBySkill(champions, skill));

        Task<int> IChampionsManager.GetNbItemsBySkill(string skill)
            => context.Champions.toPocos().GetNbItemsWithFilter(
                champions => filterBySkillSubstring(champions, skill));

        Task<Champion?> IGenericDataManager<Champion?>.UpdateItem(Champion? oldItem, Champion? newItem)
        {
            throw new Exception();
        }
            //=> (IList<Champion>)context.Champions.toPocos().UpdateItem(oldItem, newItem);
    }
}
