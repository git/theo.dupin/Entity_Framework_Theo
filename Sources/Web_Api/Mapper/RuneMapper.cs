﻿using Model;

namespace Web_Api.Mapper
{
    public static class RuneMapper
    {
        public static RuneDTO toDTO(this Rune rune)
        {
            return new RuneDTO
            {
                Name= rune.Name,
                Description= rune.Description
            };
        }
    }
}
