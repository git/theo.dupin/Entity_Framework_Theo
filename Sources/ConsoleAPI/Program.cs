﻿using Web_Api;

namespace ConsoleAPI
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using (var client = new HttpClient())
            {
                // définit l'URL de votre API
                client.BaseAddress = new Uri("https://localhost:7189/api/Champions");

                // exécute la requete HTTP GET
                HttpResponseMessage response = await client.GetAsync("");

                // vérifie si la réponse est valide
                if (response.IsSuccessStatusCode)
                {
                    // lit le contenu de la réponse
                    string result = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                }
                else
                {
                    Console.WriteLine("Erreur : " + response.StatusCode);
                }

                // exécute la requête HTTP GET pour obtenir un champion par nom
                HttpResponseMessage response1 = await client.GetAsync("/Bard");

                if (response1.IsSuccessStatusCode)
                {
                    // lit le contenu de la réponse
                    string result = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(result);
                }
                else
                {
                    Console.WriteLine("Erreur : " + response.StatusCode);
                }

                // exécute la requête HTTP POST pour ajouter un champion
                var champion = new ChampionDTO
                {
                    Name = "Ashe",
                    Bio = "La Comtesse du froid",
                };
                /*response = await client.PostAsync("/addChampion", new StringContent("application/json"));
                HttpResponseMessage responseMessage = await client.PostAsync("/addChampion", new StringContent("application/json"));*/

                // vérifie si la réponse est valide
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Le champion a été ajouté avec succès");
                }
                else
                {
                    Console.WriteLine("Erreur : " + response.StatusCode);
                }

            }
        }
    }
}