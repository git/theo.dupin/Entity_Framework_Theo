﻿using EntityFrameworkLib;
using EntityFrameworkLib.Mappers;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDataManager
{
    static class Extensions
    {
        internal static Task<IEnumerable<T?>> GetItemsWithFilterAndOrdering<T>(this IEnumerable<T> collection,
            Func<T, bool> filter, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            IEnumerable<T> temp = collection;
            temp = temp.Where(item => filter(item));
            if (orderingPropertyName != null)
            {
                var prop = typeof(T).GetProperty(orderingPropertyName!);
                if (prop != null)
                {
                    temp = descending ? temp.OrderByDescending(item => prop.GetValue(item))
                                        : temp.OrderBy(item => prop.GetValue(item));
                }
            }
            return Task.FromResult<IEnumerable<T?>>(temp.Skip(index * count).Take(count));
        }

        public static IEnumerable<Champion> toPocos(this IEnumerable<ChampionEntity> champions)
        {
            List<Champion> result = new List<Champion>();
            foreach(ChampionEntity champion in champions)
            {
                result.Add(champion.toModel());
            }
            return result;
        }

        public static IEnumerable<Skin> toPocos(this IEnumerable<SkinEntity> skins)
        {
            List<Skin> result = new List<Skin>();
            foreach(SkinEntity skin in skins)
            {
                result.Add(skin.toModel());
            }
            return result;
        }

        internal static Task<int> GetNbItemsWithFilter<T>(this IEnumerable<T> collection, Func<T, bool> filter)
        {
            return Task.FromResult(collection.Count(item => filter(item)));
        }

        internal static Task<Champion?> UpdateItem<Champion>(this IList<Champion> collection, Champion? oldItem, Champion? newItem)
        {
            if (oldItem == null || newItem == null) return Task.FromResult<Champion?>(default(Champion));

            if (!collection.Contains(oldItem))
            {
                return Task.FromResult<Champion?>(default(Champion));
            }

            collection.Remove(oldItem!);
            collection.Add(newItem!);
            return Task.FromResult<Champion?>(newItem);
        }
    }
}
