﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkLib.Mappers
{
    public static class ChampionChanger
    {
        public static ChampionEntity toEntity(this Champion champion, LolContext context)
        {
            ChampionEntity? championEntity = context.Champions.Find(champion.Name);
            if (championEntity == null)
            {
                championEntity = new()
                {
                    Name = champion.Name,
                    Bio = champion.Bio,
                    Icon = champion.Icon,
                    Class = champion.Class,
                    Image = new() { Base64 = champion.Image.Base64 }
                };
                championEntity.Skills = champion.Skills.Select(s => s.toEntity(championEntity, context)).ToList();
                championEntity.Characteristics = champion.Characteristics.Select(c => c.toEntity(championEntity, context)).ToList();
            }
            return championEntity;
        }

        public static Champion toModel(this ChampionEntity championEntity)
        {
            var champion = new Champion(championEntity.Name, championEntity.Class, championEntity.Icon, "", championEntity.Bio);
            if(championEntity.Skills != null)
            {
                foreach(var s in championEntity.Skills)
                {
                    champion.AddSkill(s.toModel());
                }
            }
            if(championEntity.Characteristics != null)
            {
                foreach(var c in championEntity.Characteristics)
                {
                    champion.AddCharacteristics(c.toModel());
                }
            }
            return champion;
        }
    }
}
