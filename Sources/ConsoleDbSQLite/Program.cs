﻿// See https://aka.ms/new-console-template for more information

using DBEntitiesWithStub;
using EntityFrameworkLib;
using Microsoft.EntityFrameworkCore;
using Model;
using System.Net;
using static Model.RunePage;

// Test d'ajout d'image dans la BDD d'un Champion avec son image
string imageUrl = "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Jax_0.jpg";
byte[] imageBytes;

using (WebClient client = new WebClient())
{
    imageBytes = client.DownloadData(imageUrl);
}

string base64Image = Convert.ToBase64String(imageBytes);

LargeImageEntity image = new LargeImageEntity
{
    Base64 = base64Image
};

string imageUrlSkinJaximus = "https://static1.millenium.org/entity_articles/9/18/19/@/1470587-jax-5-full-1.jpg", 
    imageUrlSkinBatonDivin = "https://static1.millenium.org/entity_articles/9/18/19/@/1470592-jax-10-article_image_d-1.jpg";
byte[] bytesJaximus, bytesBatonDIvin;

using (WebClient client = new WebClient())
{
    bytesJaximus = client.DownloadData(imageUrlSkinJaximus);
    bytesBatonDIvin = client.DownloadData(imageUrlSkinBatonDivin);
}

string base64ImageSkinJaximus = Convert.ToBase64String(bytesJaximus), base64ImageSkinBatonDivin = Convert.ToBase64String(bytesBatonDIvin);

LargeImageEntity imageSkinJaximus = new LargeImageEntity
{
    Base64 = base64ImageSkinJaximus
};

LargeImageEntity imageSkinBatonDivin = new LargeImageEntity
{
    Base64 = base64ImageSkinBatonDivin
};

List<SkinEntity> skins = new List<SkinEntity>
{
    new SkinEntity
    {
        Name = "Jaximus",
        Description = "Ce skin est sorti en Mai 2011",
        Icon = "Icône de Jax",
        Price = 975,
        Image = imageSkinJaximus
    },
    new SkinEntity
    {
        Name = "Jax au Bâton divin",
        Description = "Sorti en janvier 2018",
        Icon = "Icône de Jax",
        Price = 1350,
        Image = imageSkinBatonDivin
    }
};

List<CharacteristicEntity> characteristics = new List<CharacteristicEntity>
{
    new CharacteristicEntity
    {
        Name = "Attack Speed",
        Value = 2
    },
    new CharacteristicEntity
    {
        Name = "Armor",
        Value = 35
    }
};


/*List<RunePageEntity> runePages = new List<RunePageEntity>
{
    new RunePageEntity
    {
        Name = "Jax Top",
        Runes = new Dictionary<Category, RuneEntity>
        {
            { Category.Major, new RuneEntity { Name = "Tempo Mortel", Description = "Les attaques de base contre les champions ennemisoctroient un cumul pendant 6 secondes, se régénérant lors des attaques suivantes et se cumulant jusqu'à 6 fois. Gagnez ( 10 % − 15 % / 4 % − 9 % ) (basé sur le niveau) vitesse d'attaque bonus pour chaque cumul, jusqu'à ( 60 % − 90 % / 24 % − 54 % ) (basé sur le niveau) au maximum de cumuls, auquel vous gagnez également 50 bonus de portée d'attaque et augmentez le plafond de vitesse d'attaque à 10.", RuneFamily = RuneFamily.Precision } },
            { Category.Minor1, new RuneEntity { Name = "Triomphe", Description = "marquer un takedown contre un champion ennemivous soigne de 2,5 % de votre santé maximale (+ 5 % de votre santé manquante ) et vous accorde un bonus supplémentaire de 20 pièces d'or après un délai de 1 seconde.", RuneFamily = RuneFamily.Precision} },
            { Category.Minor2, new RuneEntity { Name = "Légende : alatricité", Description = "Gagnez 3 % (+ 1,5 % par pile de légende ) en vitesse d'attaque bonus , jusqu'à 18 % aux piles maximales.", RuneFamily = RuneFamily.Precision} },
            { Category.Minor3, new RuneEntity { Name = "Baroud d'honneur", Description = "Vous infligez 5% - 11% de dégâts supplémentaires aux champions quand vous avez moins de 60% de vos PV. Les dégâts max sont atteints à 30% de vos PV.", RuneFamily = RuneFamily.Precision}},
            { Category.OtherMinor1, new RuneEntity { Name = "Plaque d'os", Description = "Après avoir subi des dégâts d'un champion ennemi, les 3 prochains sorts ou attaques de ce champion vous infligent 30 - 60 pts de dégâts en moins. Durée : 1.5 sec. Délai de récupération : 55 sec.", RuneFamily = RuneFamily.Unknown} },
            { Category.OtherMinor2, new RuneEntity { Name = "Inébranlable", Description = "Vous gagnez +5% de Ténacité et +5% de résistance aux ralentissements. Ces valeurs augmentent avec vos PV manquants, jusqu'à +20% de Ténacité et +20% de résistance aux ralentissements supplémentaires. Les valeurs max sont atteintes à 30% de vos PV. ", RuneFamily = RuneFamily.Unknown} }
        }
    }
};*/


using (var context = new SQLiteLolContext())
{
    ChampionEntity jax = new ChampionEntity
    {
        Name = "Jax",
        Icon = "Icône de Jax",
        Bio = "Saijax Cail-Rynx Icath'un grandit à Icathia, une satrapie de l'empire shurimien. Dès son plus jeune âge, son père lui contait les récits de temps révolus, lorsque leur nation était encore indépendante et fière, avant que l'oppression de Shurima ne vienne les asservir. Il lui parlait des Kohari, les héros protecteurs d'Icathia et du Mage royal. Le Mage royal n'avait pas cédé face à la conquête shurimienne, mais lorsqu'il mourut au combat, ses protecteurs kohari le suivirent à l'aide d'un suicide rituel. L'empereur de Shurima exhiba les corps pourrissants des Kohari à la vue de tous. Le Mage royal, lui, fut empalé au-dessus des portes de la cité, ses os moisissant à l'air libre.",
        Class = Model.ChampionClass.Fighter,
        Image = image,
        Skins = skins,
        Characteristics = characteristics,
        //RunePages = runePages
    };
    context.Champions.Add(jax);
    context.SaveChanges();
}

public class SQLiteLolContext : LolContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        if (!options.IsConfigured)
        {
            options.UseSqlite($"Data Source=projet.Champions.db");
        }
    }
}