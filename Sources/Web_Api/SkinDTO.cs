﻿using Model;

namespace Web_Api
{
    public class SkinDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public float Price { get; set; }

        public LargeImage Image { get; set; }

        public Skin toModel()
        {
            return new Skin(Name, Description, Price, Image);
        }
    }
}
