﻿using EntityFrameworkLib;
using Microsoft.EntityFrameworkCore;
using Model;

namespace DBEntitiesWithStub
{
    public class ChampionsDBEntitiesWithStub : LolContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ChampionEntity>().HasData(
                new ChampionEntity { Name = "Yone", Bio = "Bio de Yone", Icon = "Icon de Yone" },
                new ChampionEntity { Name = "Akali", Bio = "Bio de Akali", Icon = "Icon de Akali" },
                new ChampionEntity { Name = "Bard", Bio = "Bio de Bard", Icon = "Icon de Bard" }
            );
        }
    }
}