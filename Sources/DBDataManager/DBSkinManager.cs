﻿using EntityFrameworkLib;
using EntityFrameworkLib.Mappers;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDataManager
{
    public class DBSkinManager : ISkinsManager
    {
        private readonly LolContext context;

        public DBSkinManager()
        {
        }

        public DBSkinManager(LolContext context)
        {
            this.context = context;
        }

        async Task<Skin?> IGenericDataManager<Skin?>.AddItem(Skin? item)
        {
            if(item == null)
            {
                return null;
            }
            var additem = await context.AddAsync<Skin?>(item);
            await context.SaveChangesAsync();
            return additem.Entity;
        }

        async Task<bool> IGenericDataManager<Skin?>.DeleteItem(Skin? item)
        {
            if(item == null)
            {
                return false;
            }
            context.Remove<Skin>(item);
            await context.SaveChangesAsync();
            return true;
        }

        Task<IEnumerable<Skin?>> IGenericDataManager<Skin?>.GetItems(int index, int count, string? orderingPropertyName, bool descending)
            => context.Skins.toPocos().GetItemsWithFilterAndOrdering(s => true, index, count, orderingPropertyName, descending);

        private static Func<Skin, Champion?, bool> filterByChampion = (skin, champion) => champion != null && skin.Champion.Equals(champion!);

        Task<IEnumerable<Skin?>> ISkinsManager.GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName, bool descending)
            => context.Skins.toPocos().GetItemsWithFilterAndOrdering(skin => filterByChampion(skin, champion), index, count, orderingPropertyName, descending);

        private static Func<Skin, string, bool> filterByName = (skin, substring) => skin.Name.Contains(substring, StringComparison.InvariantCultureIgnoreCase);

        Task<IEnumerable<Skin?>> IGenericDataManager<Skin?>.GetItemsByName(string substring, int index, int count, string? orderingPropertyName, bool descending)
            => context.Skins.toPocos().GetItemsWithFilterAndOrdering(skin => filterByName(skin, substring), index, count, orderingPropertyName, descending);

        Task<int> IGenericDataManager<Skin?>.GetNbItems()
            => context.Skins.GetNbItemsWithFilter(s => true);

        Task<int> ISkinsManager.GetNbItemsByChampion(Champion? champion)
            => context.Skins.toPocos().GetNbItemsWithFilter(skin => filterByChampion(skin, champion));

        Task<int> IGenericDataManager<Skin?>.GetNbItemsByName(string substring)
            => context.Skins.toPocos().GetNbItemsWithFilter(skin => filterByName(skin, substring));

        Task<Skin?> IGenericDataManager<Skin?>.UpdateItem(Skin? oldItem, Skin? newItem)
        {
            throw new Exception();
        }
           // => (IList<Champion>)context.Skins.toPocos().UpdateItem(oldItem, newItem);
    }
}
