using EntityFrameworkLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Model;
using Web_Api.Controllers;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<LolContext>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetService<LolContext>();
    context.Database.EnsureCreated();
}

builder.Services.AddHttpClient();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
// Add versioning middleware
builder.Services.AddApiVersioning(options =>
{
    // Add a default version
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ReportApiVersions = true;
});

//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();

// Add Swagger
builder.Services.AddSwaggerGen(c =>
{
    // Add version 1 documentation
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "API_LOL", Version = "v1" });

    // Add version 2 documentation
    c.SwaggerDoc("v2", new OpenApiInfo { Title = "API_LOL", Version = "v2" });
});

app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    //app.UseSwaggerUI();
    app.UseSwaggerUI(c =>
    {
        // Configure Swagger for version 1
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "API_LOL v1");
        // Configure Swagger for version 2
        c.SwaggerEndpoint("/swagger/v2/swagger.json", "API_LOL v2");
    });
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseAuthorization();

// Map version 1 to /api/v1
app.Map("/api/v1", app =>
{
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
});

// Map version 2 to /api/v2
app.Map("/api/v2", app =>
{
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
});

//app.MapControllers();

app.Run();
