﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkLib.Mappers
{
    public static class CharacteristicChanger
    {
        public static CharacteristicEntity toEntity(this KeyValuePair<string, int> item, ChampionEntity championEntity, LolContext context)
        {
            var characteristicEntity = new CharacteristicEntity();
            if(characteristicEntity == null)
            {
                return new()
                {
                    Name = item.Key,
                    Value = item.Value,
                    ChampionForeignKey = championEntity.Name
                };
            }
            return characteristicEntity;
        }

        public static Tuple<string, int> toModel(this CharacteristicEntity characteristicEntity)
         => new(characteristicEntity.Name, characteristicEntity.Value); 
    }
}
